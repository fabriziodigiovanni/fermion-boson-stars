#include <iostream>
#include <fstream>
#include <math.h>
#include <iomanip>
#include <string>
#include <sys/stat.h>
using namespace std;

//README: 1)after finding a solution with Shoot.cpp and Solve.cpp, use as Input_line.txt the values of phi0, rho0, omega_shooting, R that you found with Shoot.cpp, and this code will build a line in the existence plot.
//NOTE: it's better to start with solutions as close as possible to the mass you want to find. If you start too far, the code might get blocked in a local minimum/maximum of the mass
//2)in the first step the code is going to use +dphi. Then at the second step it looks if the mass is moving closer to the one we are looking for; if not then is going to choose -dphi, if yes it's going to keep using +dphi.

int num;
long double y[5], y_axi[5], diff[5], F[5];
long double** var;
long double k1[5], k2[5], k3[5], k4[5];
const long double Pi=3.14159265358979323846L, lambda= 0.0*4*Pi, m=1.0, KK=100, polind = 2;
long double R;
long double eps;
long double Cutphi, CutP;
long double omega2;
long double delta_r, r;
int sign (long double z) {
    if (z>0) return 1;
    if (z==0) return 0;
    if (z<0) return -1;
}
long double V(long double phi) {
    return pow(m,2) * pow(phi,2) + 0.5*lambda*pow(phi,4);
}
long double dV(long double phi) {
    return pow(m,2) + lambda*pow(phi,2);
}
int func(long double r,  long double omega2, long double h) {
    long double a=y_axi[0];
    long double alp=y_axi[1];
    long double phi=y_axi[2];
    long double dphi=y_axi[3];
    long double P=y_axi[4];
//OSS: eps=K*rho --> eps=10*sqrt(P) eps = pow(KK,1/polind)*pow(P,(polind-1)/polind)/(polind-1)
    eps = pow(KK,1/polind)*pow(P,(polind-1)/polind)/(polind-1);
    F[0] = 0.5*a*((1-a*a)/r + 4*Pi*r*( pow(dphi,2) + omega2 * pow(a*phi/alp, 2) + a*a * V(phi)+2*a*a*0.1*sqrt(P)*(1+eps)));
    F[1] = 0.5*alp*((a*a-1)/r + 4*Pi*r*( pow(dphi,2) + omega2 * pow(a*phi/alp, 2) - a*a * V(phi)+2*a*a*P));
    F[2] = dphi;
    F[3] = -(1+a*a-4*Pi*pow(r*a,2)*(V(phi)+0.1*sqrt(P)*(1+eps)-P))*dphi/r+(dV(phi)-omega2/pow(alp,2))* a*a * phi;
    F[4] =-(0.1*sqrt(P)*(1+eps)+P)*0.5*((a*a-1)/r + 4*Pi*r*( pow(dphi,2) + omega2 * pow(a*phi/alp, 2) - a*a * V(phi)+2*a*a*P));

    return 0;
}

int Runge_Kutta_4(long double r,  long double omega2, long double h)  {
    long double half_h=0.5*h;

    for (int i=0; i<=4; i++) y_axi[i]=y[i];
    func(r, omega2, h);
    for (int i=0; i<=4; i++) k1[i]=h*F[i];
    for (int i=0; i<=4; i++) y_axi[i]=y[i]+0.5*k1[i];
//control for P not to become negative
        if (y_axi[4]<=0) {
		y_axi[4]=0;
		k1[4]=0;
		}
    func(r+half_h, omega2, h);
    for (int i=0; i<=4; i++) k2[i]=h*F[i];
    for (int i=0; i<=4; i++) y_axi[i]=y[i]+0.5*k2[i];
        if (y_axi[4]<=0) {
		y_axi[4]=0;
		k2[4]=0;
		}
    func(r+half_h, omega2, h);
    for (int i=0; i<=4; i++) k3[i]=h*F[i];
    for (int i=0; i<=4; i++) y_axi[i]=y[i]+k3[i];
        if (y_axi[4]<=0) {
		y_axi[4]=0;
		k3[4]=0;
		}
    func(r+h, omega2, h);
    for (int i=0; i<=4; i++) k4[i]=h*F[i];
    for (int i=0; i<=4; i++) diff[i]=(k1[i]+k4[i])/6 + (k2[i]+k3[i])/3;
    return 0;
}

int ode_int(long double phi0c, long double rho0c, long double omega, long double* a){
    int n=0;
    y[0]=1;
    y[1]=1;
    y[2]=phi0c;
    y[3]=0;
    y[4]=KK*pow(rho0c,polind);
    delta_r=R/(num);
    omega2=pow(omega,2);
    r=delta_r;
    while (r<R) {
        for (int i=0; i<=4; i++) {
            var[i][n]=y[i];
        }
        n++;
        Runge_Kutta_4(r, omega2, delta_r);
	
        for (int i=0; i<=4; i++) {
           y[i]+=diff[i];
        }
//control for P not to become negative
	if(y[4]<=0) 
		y[4]=0;

	if(abs(y[4])<1.0e-10 && y[4]<0) 
		y[4] = abs(y[4]);
        if (y[2] > Cutphi ||  y[4]> CutP ){ // || y[2] < 0 || diff[2]>0) {
		break;
		}
        r+=delta_r;
    }
    *a = y[0];

    return sign(y[2]);
}

int main(int argc, char *argv[]){
	int input_num;
	//NOTE: flag==0 is +dphi, flag==1 is -dphi
	int flag=0;
	int first_time=0, good_result;
   	long double order=-19, phi0c, rho0c, dphi;
	long double mass, mass_tmp, grr;
  	int ind;
     	long double omega_in, omega_1, omega_2, omega_mid;
	long double mass_in, delta_m;
	if (argc<2){
	  cout<<"You should run './Shoot data' "<<'\n';
	  cout<<"where data is the folder where you want to put your output"<<endl;
	  return 0;
	}

   	ifstream  input;
   	ofstream  output;
	string folder;
	folder = argv[1];
	char* folder2;
	folder2 = argv[1];
	mkdir( folder2, S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH) != 0;
   	input.open("Input_line_new.txt", ios::in);
   	output.open(folder + "/Output.txt", ios::out);
   	input >> order >> num >> input_num;

	var = new long double*[5];
	for (int i = 0;i<5;i++)
		var[i] = new long double[num];


   	output << input_num << endl;
   	output << setprecision(2-order);
   	cout << setprecision(2-order); 

   	if (order<0) {int j=(-1)*order;
      		order=1;
      		for (int i=0; i<j; i++) {
			order=order*0.1;
      		}
      		cout << order << "\n";
   	}
	int j=0;
	input >> mass_in;

	dphi = 2e-4;

	//this is the precision I want to have about the mass. Let's say 1% error
	delta_m = mass_in*1e-3;

   	for (int j=0; j<input_num; j++) {

     	  input >> phi0c >> rho0c>> omega_in >> R;
	  mass = 0.;
	  mass_tmp=0.;
	  first_time = 0.;


	  while(mass>(mass_in+delta_m) || mass<(mass_in - delta_m)){
     		cout << j <<" "<<phi0c<<" "<<rho0c<< endl;
		good_result=0;
	        ind = 0;
		//I set here the cuts for phi and P..if the solution goes over these values it breaks. To avoid having nans.
		Cutphi = 1.1*phi0c;
		CutP = 1.1* KK*pow(rho0c,polind);
		//try to enlarge the range of omega until you find a mass that makes sense. 
		//This is because the result is quite sensitive to the guess for the shooting and sometimes it fails to find 
		//the solution but if you change slightly the guess he can find it...
		while(good_result==0){
			ind++;
			omega_1 = pow(0.9998,ind)*omega_in;
			omega_2 = pow(1.0002,ind)*omega_in;

     			long double f_1=ode_int(phi0c, rho0c, omega_1, &grr);
     			long double f_2=ode_int(phi0c, rho0c, omega_2, &grr);
     			long double f_mid;
			//if I used +dphi so omega is going to be bigger than the previous step so I look on the right of last omega. 
			// if I used -dphi is the other way around
     			while (f_1*f_2>0) {
				
        			omega_2=omega_2*1.005;
        			omega_1=omega_1/1.005;

        			f_1=ode_int(phi0c, rho0c, omega_1, &grr);
        			f_2=ode_int(phi0c, rho0c, omega_2, &grr);

     			}

     			while((omega_2-omega_1)/omega_1>2*order) {
        			omega_mid=(omega_1+omega_2)/2;
        			cout << omega_mid << "  ";
        			f_mid=ode_int(phi0c, rho0c, omega_mid, &grr);
			
        			if (f_mid>0){
					if(f_1<0){
						omega_2=omega_mid;
						f_2=f_mid;
					}
					else {
						omega_1=omega_mid;
						f_1=f_mid;
					}
				}
				else{
					if(f_1<0){
						omega_1=omega_mid;
						f_1=f_mid;
					}
					else {
						omega_2=omega_mid;
						f_2=f_mid;
					}
				}

			}
			cout<<endl;
			mass = (R/2.) * (1-1/pow(grr,2));

		//at the first iteration I don't want the while to act (I suppose the first time is going to give me a good result)
			if(first_time==0){
				good_result = 1;
				first_time=1;
				}
			if(abs(mass-mass_tmp)<0.1)
				good_result=1;
		}//end while good result

		cout<<"mass"<<mass<<endl;
		cout<<"phi0c = "<<phi0c<<" rho0c= "<<rho0c<<" mass= "<<mass<<endl;


		//NOTE: 1) if mass_tmp was higher than (mass_in+delta_m) and the mass of the new step is decreasing, this means in the last
		// iteration I chose the right dphi, so let's continue with it, if the mass is increasing change sign of dphi
		//2) if mass_tmp was lower than (mass_in-delta_m) and the mass of the new step is INCREASING, this means in the last
		// iteration I chose the right dphi, so let's continue with it, if the mass is DECREASING change sign of dphi

		if(mass_tmp > (mass_in + delta_m)){
			if((mass-mass_tmp)<0){
				if(flag==1){
					phi0c = phi0c - dphi;
					flag =1;
				}else{
					phi0c = phi0c + dphi;
					flag =0;
				}
			}else if((mass-mass_tmp)>0){
				if(flag==1){
					phi0c = phi0c + dphi;
					flag =0;
				}else{
					phi0c = phi0c - dphi;
					flag =1;
				}
			
			}else if((mass-mass_tmp)==0){
				cout<<"LOCAL MINIMUM MASS REACHED"<<endl;
				break;
			}

		}else if (mass_tmp < (mass_in - delta_m)){
			if((mass-mass_tmp)<0){
				if(flag==1){
					phi0c = phi0c + dphi;
					flag =0;
				}else{
					phi0c = phi0c - dphi;
					flag =1;
				}
			}else if((mass-mass_tmp)>0){
				if(flag==1){
					phi0c = phi0c -dphi;
					flag =1;
				}else{
					phi0c = phi0c + dphi;
					flag =0;
				}
			
			}else if((mass-mass_tmp)==0){
				cout<<"LOCAL MINIMUM MASS REACHED"<<endl;
				break;
			}
		}

		mass_tmp = mass;

		//New value for the shooting, use the solution of the last shooting as new guess for the new one.
		// omega for next step is going to be higher if I used +dphi, lower if I used -dphi
		if(flag==1)
			omega_in = 0.9999*omega_mid;
		else
			omega_in = 1/0.9999*omega_mid;


	   } //end of while

	 //This is because before the while ended, I added/subtracted dphi, but the last solution
	// created was for the previous value of phi0c, so I subtract/add dphi before printing
	   if(flag==1)
		phi0c = phi0c + dphi;
	   else
		phi0c = phi0c - dphi;

	   output <<  phi0c << " " << KK*pow(rho0c,polind) << " " << omega_mid << " "<< num <<" "<< R << endl;

	} //end of for




	input.close();
	output.close();
	return 0;
}
