This code was created by Fabrizio Di Giovanni (University of Valencia - Valencia(Spain)) and Saeed Fakhry (Shahid Beheshti University - Teheran (Iran)). This code had been used to reproduce initial data for fermion-boson stars reported in the papers:

arXiv:2006.08583
arXiv:2105.00530

Contact email: fabrizio2.giovanni@uv.es

Brief tutorial:
------------------------------------
        DESCRIPTION OF THE CODE
------------------------------------

The code structure is the following:

- Input.txt

  This is the input file used to run Shoot.cpp. It contains:
  tolerance you want to reach (negative number n --> tolerance=10^n)
  number of points
  number of models you want to create
  phi0c (central value scalar field) rho0c (central value fluid rest-mass density) R (radius of the outer boundary)

- Shoot.cpp

  It takes as inputs the file Input.txt and it evaluates the eigenfrequency omega of the scalar field, using a shooting method. It prints a file Output.txt that is used to run Solve.cpp

- Solve.cpp

  Takes the value of omega_shooting and it reconstruct the solution and print different radial profiles and info on the model in files.

------------------------------------
        HOW TO RUN THE CODE
------------------------------------

- Compile both codes running:

   g++ -o Shoot Shoot.cpp
   
   g++ -o Solve Solve.cpp


- Run Shoot:
  ./Shoot data_folder
  
  where data_folder is the folder where you want to save all the output of the model you wants to generate

- Run Solve:
  ./Solve data_folder



Have fun!
